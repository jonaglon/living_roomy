
/*__      ___   __   __  ___   __    _  _______   ______    _______  _______  __   __   ___      ___   _______  __   __  _______  _______ 
|   |    |   | |  | |  ||   | |  |  | ||       | |    _ |  |       ||       ||  |_|  | |   |    |   | |       ||  | |  ||       ||       |
|   |    |   | |  |_|  ||   | |   |_| ||    ___| |   | ||  |   _   ||   _   ||       | |   |    |   | |    ___||  |_|  ||_     _||  _____|
|   |    |   | |       ||   | |       ||   | __  |   |_||_ |  | |  ||  | |  ||       | |   |    |   | |   | __ |       |  |   |  | |_____ 
|   |___ |   | |       ||   | |  _    ||   ||  | |    __  ||  |_|  ||  |_|  ||       | |   |___ |   | |   ||  ||       |  |   |  |_____  |
|       ||   |  |     | |   | | | |   ||   |_| | |   |  | ||       ||       || ||_|| | |       ||   | |   |_| ||   _   |  |   |   _____| |
|_______||___|   |___|  |___| |_|  |__||_______| |___|  |_||_______||_______||_|   |_| |_______||___| |_______||__| |__|  |___|  |_______|        */

#include<Arduino.h>
#include<Wire.h>
#include<Keypad.h>
#include<FastLED.h>

const bool testMode = false;

const byte ROWS = 4; //four rows
const byte COLS = 3; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
 {'1','2','3'},
 {'4','5','6'},
 {'7','8','9'},
 {'*','0','#'}
};

byte rowPins[ROWS] = {52, 50, 48, 46}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {44, 42, 40}; //connect to the column pinouts of the keypad
//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

unsigned long totalTimey;
int cycle, currentCycle, timey, slowTimey, vSlowTimey, animLength;

int keypadCounter;

const int numLedsStrip = 684;
const int numLedsTotal = numLedsStrip * 4;
CRGB rgbwLeds[numLedsTotal]; // 600 * 4

byte wheelR;
byte wheelG;
byte wheelB;

byte userColR=40;
byte userColG=20;
byte userColB=20;


void setup() {
  if (testMode) {
    Serial.begin(9600);
    Serial.println(" * * Test Mode Start * * ");
  }
  
  // Make random more random
  randomSeed(analogRead(0));

  currentCycle=0;
  animLength=16384; // 32768;

  LEDS.addLeds<WS2811_PORTD, 4>(rgbwLeds, numLedsStrip); // Hardcoded to ports:25,26,27,28,14,15
  LEDS.setBrightness(100); // 128 good max, 255 actual /max

  setupTwinkles(2);
}

void loop() {

  setTimes();

  //allOneColor(1,1,1);
  //LEDS.show();
  allOff1();

  doKeypad();

  doLights();

  LEDS.show();

}

bool pastHalf=false;

void setTimes() {
  totalTimey = millis();
  cycle = totalTimey / animLength;
  timey = totalTimey % animLength;
  slowTimey = (totalTimey / 10) % animLength;
  vSlowTimey = (totalTimey / 100) % animLength;

}



int ledSections[16] = {
  0,     // 0  bottom ring *
  171,   // 1  big heart
  342,   // 2  small heart
  513,   // 3 underarm left
  684,   // 4 overarm left
  855,   // 5  eye left
  1026,   // 6  eye right
  1197,   // 7  mouth
  1368,   // 8  tape
  1539,   // 9  tuner *
  1710,   // 10 indiciator *
  1881,   // 11 underarm right
  2052,   // 12 overarm right
  2223,   // 13 tube bottomright *
  2395,   // 14 tube bottomleft *
  2565,  // 15 tube topleft *
};



struct twinkle {
  short ledNum;
  byte rCol;
  byte gCol;
  byte bCol;
  byte rToCol;
  byte gToCol;
  byte bToCol;
  int start;
  short lengthy;
  short widthy;
  short fadeIn;
  short fadeOut;
  short speedy;
  short sideFade;

  twinkle(short aLedNum, byte aRCol, byte aGCol, byte aBCol, byte aToRCol, byte aToGCol, byte aToBCol, int aStart, short aLengthy, short aWidthy, short aFadeIn, short aFadeOut, short aSpeedy, short aSideFade) :
    ledNum(aLedNum), rCol(aRCol), gCol(aGCol), bCol(aBCol), rToCol(aToRCol), gToCol(aToGCol), bToCol(aToBCol), start(aStart), lengthy(aLengthy), widthy(aWidthy), fadeIn(aFadeIn), fadeOut(aFadeOut), speedy(aSpeedy), sideFade(aSideFade) {  }

  twinkle() : ledNum(0), rCol(0), gCol(0), bCol(0), start(0), lengthy(0), widthy(0), fadeIn(0), fadeOut(0), speedy(0), sideFade(0) { }

};

const int numTwinks = 3000;
twinkle myTwinkles[numTwinks];
const int usedTwinkleCount[] = {3000, 3, 3, 300, 300, 500, 500, 500, 1000};


