
void allOff1() {
  for(int j = 0; j < numLedsTotal; j++) {
    setLedDirect(j, 0, 0, 0, 0);
  }
}

void allOff2() {
  for(int j = 0; j < numLedsStrip; j++) {
    setLedDirectStrip(j, 0, 0, 0, 0);
  }
}

void allOn() {
  for(int j = 0; j < numLedsTotal; j++) {
    setLedDirect(j, 40, 20, 20, 0);
  }
}

void allOneColor(int r, int g, int b) {
  for(int j = 0; j < numLedsTotal; j++) {
      setLedDirect(j, r, g, b, 0);
  }
}

void setLedDirect(int ledNum, int rVal, int gVal, int bVal, int wVal) {
    int whitey = wVal/3;
    rgbwLeds[ledNum].r = (rVal + whitey) % 255;
    rgbwLeds[ledNum].g = (gVal + whitey) % 255;
    rgbwLeds[ledNum].b = (bVal + whitey) % 255;
}

void setLightFade(int ledNum, int cycleLength, int percentThroughCycle, int rStartValue, int gStartValue, int bStartValue) {
  int rVal = ((percentThroughCycle*100 / cycleLength) * rStartValue)/100;
  int gVal = ((percentThroughCycle*100 / cycleLength) * gStartValue)/100;
  int bVal = (((percentThroughCycle*100) / cycleLength) * bStartValue)/100;
  setLedDirectStrip(ledNum, rVal, gVal, bVal, 0);
}

void setLedDirectStrip(int ledNum, int rVal, int gVal, int bVal, int wVal) {
    if (rVal < 0)
      rVal = 0;
    if (gVal < 0)
      gVal = 0;
    if (bVal < 0)
      bVal = 0;
    if (wVal < 0)
      wVal = 0;
    if (ledNum < 0)
      ledNum = 0;

    ledNum = ledNum%numLedsStrip;
    int whitey = wVal/3;

    rgbwLeds[ledNum].r = (rVal + whitey) % 255;
    rgbwLeds[ledNum].g = (gVal + whitey) % 255;
    rgbwLeds[ledNum].b = (bVal + whitey) % 255;

    rgbwLeds[numLedsStrip + ledNum].r = (rVal + whitey) % 255;
    rgbwLeds[numLedsStrip + ledNum].g = (gVal + whitey) % 255;
    rgbwLeds[numLedsStrip + ledNum].b = (bVal + whitey) % 255;

    rgbwLeds[(numLedsStrip*2) + ledNum].r = (rVal + whitey) % 255;
    rgbwLeds[(numLedsStrip*2) + ledNum].g = (gVal + whitey) % 255;
    rgbwLeds[(numLedsStrip*2) + ledNum].b = (bVal + whitey) % 255;

    rgbwLeds[(numLedsStrip*3) + ledNum].r = (rVal + whitey) % 255;
    rgbwLeds[(numLedsStrip*3) + ledNum].g = (gVal + whitey) % 255;
    rgbwLeds[(numLedsStrip*3) + ledNum].b = (bVal + whitey) % 255;
}



/*void setLedDirectAll(int ledNum, int rVal, int gVal, int bVal, int wVal) {
    int whitey = wVal/3;
    rgbwLeds[ledNum].r = rVal + whitey;
    rgbwLeds[ledNum].g = gVal + whitey;
    rgbwLeds[ledNum].b = bVal + whitey;

    rgbwLeds[ledNum*2].r = rVal + whitey;
    rgbwLeds[ledNum*2].g = gVal + whitey;
    rgbwLeds[ledNum*2].b = bVal + whitey;

    rgbwLeds[ledNum*3].r = rVal + whitey;
    rgbwLeds[ledNum*3].g = gVal + whitey;
    rgbwLeds[ledNum*3].b = bVal + whitey;

    rgbwLeds[ledNum*4].r = rVal + whitey;
    rgbwLeds[ledNum*4].g = gVal + whitey;
    rgbwLeds[ledNum*4].b = bVal + whitey;
}*/


int getCoord(int ledNum, int xOrY) {
  if (ledNum < 2003)
     return 10;
/*  else if (ledNum < 378)
    return bigHeartCoords[ledNum-203][xOrY]+ledPosOffset[1][xOrY];
  else if (ledNum < 463)
    return smHeartCoords[ledNum-378][xOrY]+ledPosOffset[2][xOrY];
  else if (ledNum < 482)
    return armCoords[ledNum-463][xOrY]+ledPosOffset[3][xOrY];
  else if (ledNum < 506)
    return armCoords[ledNum-482][xOrY]+ledPosOffset[4][xOrY];
  else if (ledNum < 599)
    return eyeCoords[ledNum-506][xOrY]+ledPosOffset[5][xOrY];
  else if (ledNum < 692)
    return eyeCoords[ledNum-599][xOrY]+ledPosOffset[6][xOrY];
  else if (ledNum < 710)
    return horizCoords[ledNum-692][xOrY]+ledPosOffset[7][xOrY];
  else if (ledNum < 744)
    return tapeCoords[ledNum-710][xOrY]+ledPosOffset[8][xOrY];
  else if (ledNum < 770)
    return horizCoords[ledNum-744][xOrY]+ledPosOffset[9][xOrY];
  else if (ledNum < 774)
    return horizCoords[ledNum-770][xOrY]+ledPosOffset[10][xOrY];
  else if (ledNum < 797)
    return armCoords[ledNum-774][xOrY]+ledPosOffset[11][xOrY];
  else if (ledNum < 821)
    return armCoords[ledNum-797][xOrY]+ledPosOffset[12][xOrY];
  else if (ledNum < 911)
    return tubeCoords[ledNum-821][xOrY]+ledPosOffset[13][xOrY];
  else if (ledNum < 1001)
    return tubeCoords[ledNum-911][xOrY]+ledPosOffset[14][xOrY];
  else if (ledNum < 1090)
    return tubeCoords[ledNum-1001][xOrY]+ledPosOffset[15][xOrY];
  else if (ledNum < 1179)
    return tubeCoords[ledNum-1090][xOrY]+ledPosOffset[16][xOrY];
  else if (ledNum < 1302)
    return portLCoords[(ledNum-1179)%19][xOrY]+ledPosOffset[17][xOrY];
  else if (ledNum < 1441)
    return portRCoords[(ledNum-1302)%19][xOrY]+ledPosOffset[18][xOrY];  */
}


int quickAbsolute(int number) {
  if (number < 0)
    return number * (-1);
  else
    return number;
}



void SetRgbwWheelVars(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    wheelR = 255 - WheelPos * 3;
    wheelG = 0;
    wheelB = WheelPos * 3;
    return;
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    wheelR = 0;
    wheelG = WheelPos * 3;
    wheelB = 255 - WheelPos * 3;
    return;
  }
  WheelPos -= 170;
  wheelR = WheelPos * 3;
  wheelG = 255 - WheelPos * 3;
  wheelB = 0;
  return;
}

int goodColR, goodColG, goodColB, goodColW;
void setGoodRandomColorVars() {
  int randomNum = random(4);
  switch (randomNum) {
    case 0: 
      goodColR = 255;
      goodColG = 0;
      goodColB = 0;
      goodColW = 0;
      break;
    case 1: 
      goodColR = 0;
      goodColG = 255;
      goodColB = 0;
      goodColW = 0;
      break;
    case 2: 
      goodColR = 0;
      goodColG = 0;
      goodColB = 255;
      goodColW = 0;
      break;
    case 3: 
      goodColR = 0;
      goodColG = 0;
      goodColB = 0;
      goodColW = 255;
      break;
  }
}

int sectionsR;
int sectionsG;
int sectionsB;
void doSections() {
  

  for(int j = 0; j < numLedsTotal; j++) {
    if (j > ledSections[15]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[14]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[13]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[12]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[11]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[10]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[9]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[8]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[7]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[6]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[5]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[4]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[3]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[2]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[1]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    } else if (j > ledSections[0]) {
      setLedDirect(j, sectionsR, sectionsG, sectionsB, 0);
    }
  }
}


