const short speedDivisor = 70;  /* small numbers equals faster strips! */
int currentTwinklePattern = 5;
int ledToLight;

void doTwinkles() {

  if (cycle != currentCycle) {
    setupTwinkles(currentPattern);
    currentCycle = cycle;
  }

  for (int twinky = 0; twinky < usedTwinkleCount[currentTwinklePattern]; twinky++) {

    int ticky = (timey / myTwinkles[twinky].speedy)/speedDivisor;
    int newLedNum = quickAbsolute(myTwinkles[twinky].ledNum + ticky);
    int twinkLength = myTwinkles[twinky].fadeIn+myTwinkles[twinky].lengthy+myTwinkles[twinky].fadeOut; 
    int percentThroughCycle = (timey-(myTwinkles[twinky].start-myTwinkles[twinky].fadeIn))*100;
    int bigDividePercent = percentThroughCycle / twinkLength;    

    if ((timey > (myTwinkles[twinky].start-myTwinkles[twinky].fadeIn)) && (timey < myTwinkles[twinky].start)) { 
      if (testMode) {
        Serial.print("newLedNum:");
        Serial.print(newLedNum);
        Serial.print(" twinky:");
        Serial.print(twinky);
        Serial.print(" bigDividePercent:");
        Serial.println(bigDividePercent);
      }
      setLedsFadeIn(newLedNum, twinky, bigDividePercent);
    } else if ((timey >= myTwinkles[twinky].start) && (timey < (myTwinkles[twinky].start + myTwinkles[twinky].lengthy))) { 
      if (testMode) {
        Serial.print("newLedNum:");
        Serial.print(newLedNum);
        Serial.print(" twinky:");
        Serial.print(twinky);
        Serial.print(" bigDividePercent:");
        Serial.println(bigDividePercent);
      }
      setLedsDirect(newLedNum, twinky, bigDividePercent);
    } else if ((timey >= (myTwinkles[twinky].start + myTwinkles[twinky].lengthy)) && (timey < twinkLength) { 
      if (testMode) {
        Serial.print("newLedNum:");
        Serial.print(newLedNum);
        Serial.print(" twinky:");
        Serial.print(twinky);
        Serial.print(" bigDividePercent:");
        Serial.println(bigDividePercent);
      }
      setLedsFadeOut(newLedNum, twinky, bigDividePercent);
    } else if (timey < twinkLength) { 
      if (testMode) {
        Serial.print("RESET:");
        Serial.print(" twinky:");
        Serial.print(twinky);
        Serial.print(" bigDividePercent:");
        Serial.println(bigDividePercent);
      }
      resetTwinkle(twinky, bigDividePercent);
    }
  }
}

void setupTwinkles(int newTwinklePattern) {
  currentTwinklePattern = newTwinklePattern;
  switch (newTwinklePattern) {
    case 1: 
      setupTwinkles1();
      break;
    case 2: 
      setupTwinkles2();
      break;
    case 3: 
      setupTwinkles3();
      break;
    case 4: 
      setupTwinkles4();
      break;
    case 5: 
      setupTwinkles5();
      break;
    case 6: 
      setupTwinkles6();
      break;
  }  
}


void setLedsFadeIn(int ledNum, int twinky, int bigDividePercent) {
  int newRed = myTwinkles[twinky].rCol+(((myTwinkles[twinky].rToCol-myTwinkles[twinky].rCol)*bigDividePercent)/100);
  int newGreen = myTwinkles[twinky].gCol+(((myTwinkles[twinky].gToCol-myTwinkles[twinky].gCol)*bigDividePercent)/100);
  int newBlue = myTwinkles[twinky].bCol+(((myTwinkles[twinky].bToCol-myTwinkles[twinky].bCol)*bigDividePercent)/100);

  if (rgbwLeds[ledNum].r > newRed) { newRed = myTwinkles[twinky].rCol; }
  if (rgbwLeds[ledNum].g > newGreen) { newGreen = myTwinkles[twinky].gCol; } 
  if (rgbwLeds[ledNum].b > newBlue) { newBlue = myTwinkles[twinky].bCol; } 

  for (int ledToLight = 0; ledToLight < myTwinkles[twinky].widthy; ledToLight++) {
    setLightFade(ledNum+ledToLight, myTwinkles[twinky].fadeIn, timey-(myTwinkles[twinky].start-myTwinkles[twinky].fadeIn), newRed-(myTwinkles[twinky].sideFade*ledToLight), newGreen-(myTwinkles[twinky].sideFade*ledToLight), newBlue-(myTwinkles[twinky].sideFade*ledToLight));
  }  
  for (int ledToLight = 0; ledToLight < myTwinkles[twinky].widthy; ledToLight++) {
    setLightFade(ledNum-ledToLight, myTwinkles[twinky].fadeIn, timey-(myTwinkles[twinky].start-myTwinkles[twinky].fadeIn), newRed-(myTwinkles[twinky].sideFade*ledToLight), newGreen-(myTwinkles[twinky].sideFade*ledToLight), newBlue-(myTwinkles[twinky].sideFade*ledToLight));
  } 
}

void setLedsDirect(int ledNum, int twinky, int bigDividePercent) {
  int newRed = myTwinkles[twinky].rCol+(((myTwinkles[twinky].rToCol-myTwinkles[twinky].rCol)*bigDividePercent)/100);
  int newGreen = myTwinkles[twinky].gCol+(((myTwinkles[twinky].gToCol-myTwinkles[twinky].gCol)*bigDividePercent)/100);
  int newBlue = myTwinkles[twinky].bCol+(((myTwinkles[twinky].bToCol-myTwinkles[twinky].bCol)*bigDividePercent)/100);

  if (rgbwLeds[ledNum].r > newRed) { newRed = myTwinkles[twinky].rCol; } 
  if (rgbwLeds[ledNum].g > newGreen) { newGreen = myTwinkles[twinky].gCol; } 
  if (rgbwLeds[ledNum].b > newBlue) { newBlue = myTwinkles[twinky].bCol; } 

  for (int ledToLight = 0; ledToLight < myTwinkles[twinky].widthy; ledToLight++) {
    setLedDirectStrip(ledNum+ledToLight, newRed-(myTwinkles[twinky].sideFade*ledToLight), newGreen-(myTwinkles[twinky].sideFade*ledToLight), newBlue-(myTwinkles[twinky].sideFade*ledToLight), 0);
  }  
  for (int ledToLight = 0; ledToLight < myTwinkles[twinky].widthy; ledToLight++) {
    setLedDirectStrip(ledNum-ledToLight, newRed-(myTwinkles[twinky].sideFade*ledToLight), newGreen-(myTwinkles[twinky].sideFade*ledToLight), newBlue-(myTwinkles[twinky].sideFade*ledToLight), 0);
  }
}

void setLedsFadeOut(int ledNum, int twinky, int bigDividePercent) {
  int newRed = myTwinkles[twinky].rCol+(((myTwinkles[twinky].rToCol-myTwinkles[twinky].rCol)*bigDividePercent)/100);
  int newGreen = myTwinkles[twinky].gCol+(((myTwinkles[twinky].gToCol-myTwinkles[twinky].gCol)*bigDividePercent)/100);
  int newBlue = myTwinkles[twinky].bCol+(((myTwinkles[twinky].bToCol-myTwinkles[twinky].bCol)*bigDividePercent)/100);

  if (rgbwLeds[ledNum].r > newRed) { newRed = myTwinkles[twinky].rCol; }
  if (rgbwLeds[ledNum].g > newGreen) { newGreen = myTwinkles[twinky].gCol; } 
  if (rgbwLeds[ledNum].b > newBlue) { newBlue = myTwinkles[twinky].bCol; } 

  for (int ledToLight = 0; ledToLight < myTwinkles[twinky].widthy; ledToLight++) {
    setLightFade(ledNum+ledToLight, myTwinkles[twinky].fadeOut, (myTwinkles[twinky].start+myTwinkles[twinky].lengthy+myTwinkles[twinky].fadeOut)-timey, newRed-(myTwinkles[twinky].sideFade*ledToLight), newGreen-(myTwinkles[twinky].sideFade*ledToLight), newBlue-(myTwinkles[twinky].sideFade*ledToLight));
  }  
  for (int ledToLight = 0; ledToLight < myTwinkles[twinky].widthy; ledToLight++) {
    setLightFade(ledNum-ledToLight, myTwinkles[twinky].fadeOut, (myTwinkles[twinky].start+myTwinkles[twinky].lengthy+myTwinkles[twinky].fadeOut)-timey, newRed-(myTwinkles[twinky].sideFade*ledToLight), newGreen-(myTwinkles[twinky].sideFade*ledToLight), newBlue-(myTwinkles[twinky].sideFade*ledToLight));
  } 
}

void setupTwinkles1() {
  for (int twinky = 0; twinky < usedTwinkleCount[currentTwinklePattern]; twinky++) {
    myTwinkles[twinky].ledNum = random(numLedsStrip);
    myTwinkles[twinky].rCol =  190;
    myTwinkles[twinky].gCol =  190;
    myTwinkles[twinky].bCol =  190;
    myTwinkles[twinky].rToCol =  myTwinkles[twinky].rCol;
    myTwinkles[twinky].gToCol =  myTwinkles[twinky].gCol;
    myTwinkles[twinky].bToCol =  myTwinkles[twinky].bCol;
    myTwinkles[twinky].start =  random(animLength);
    myTwinkles[twinky].lengthy = random(50,150);
    myTwinkles[twinky].widthy =  1;
    myTwinkles[twinky].fadeIn =  0;
    myTwinkles[twinky].fadeOut =  0;
    myTwinkles[twinky].speedy = 0;
    myTwinkles[twinky].sideFade = 0;
  }
}


void setupTwinkles2() {
  for (int twinky = 0; twinky < usedTwinkleCount[currentTwinklePattern]; twinky++) {
    myTwinkles[twinky].ledNum = random(numLedsStrip);
    myTwinkles[twinky].rCol =  random(200);
    myTwinkles[twinky].gCol =  random(180);
    myTwinkles[twinky].bCol =  random(200);
    myTwinkles[twinky].rToCol =  myTwinkles[twinky].rCol;
    myTwinkles[twinky].gToCol =  myTwinkles[twinky].gCol;
    myTwinkles[twinky].bToCol =  myTwinkles[twinky].bCol;
    myTwinkles[twinky].start =  random(animLength);
    myTwinkles[twinky].lengthy = random(1000,4000);
    myTwinkles[twinky].widthy =  1;
    myTwinkles[twinky].fadeIn =  0;
    myTwinkles[twinky].fadeOut =  0;
    myTwinkles[twinky].speedy = 0;
    myTwinkles[twinky].sideFade = 0;
  }
}


void setupTwinkles3() {
  for (int twinky = 0; twinky < usedTwinkleCount[currentTwinklePattern]; twinky++) {
    myTwinkles[twinky].ledNum = random(numLedsTotal);
    myTwinkles[twinky].rCol =  random(220);
    myTwinkles[twinky].gCol =  random(200);
    myTwinkles[twinky].bCol =  random(210);
    myTwinkles[twinky].rToCol =  myTwinkles[twinky].rCol;
    myTwinkles[twinky].gToCol =  myTwinkles[twinky].gCol;
    myTwinkles[twinky].bToCol =  myTwinkles[twinky].bCol;
    myTwinkles[twinky].start =  random(animLength);
    myTwinkles[twinky].lengthy =  random(1500,5000);
    myTwinkles[twinky].widthy =  1;
    myTwinkles[twinky].fadeIn =  1000;
    myTwinkles[twinky].fadeOut =  1200;
    myTwinkles[twinky].speedy = 0;
    myTwinkles[twinky].sideFade = 0;
  }
}


void setupTwinkles4() {
  for (int twinky = 0; twinky < usedTwinkleCount[currentTwinklePattern]; twinky++) {
    myTwinkles[twinky].ledNum = random(numLedsStrip);
    myTwinkles[twinky].rCol =  random(200);
    myTwinkles[twinky].gCol =  random(100);
    myTwinkles[twinky].bCol =  random(150);
    myTwinkles[twinky].rToCol =  myTwinkles[twinky].rCol;
    myTwinkles[twinky].gToCol =  myTwinkles[twinky].gCol;
    myTwinkles[twinky].bToCol =  myTwinkles[twinky].bCol;
    myTwinkles[twinky].start =  random(animLength);
    myTwinkles[twinky].lengthy = random(2000,5000);
    myTwinkles[twinky].widthy =  random(3,23);
    myTwinkles[twinky].fadeIn =  random(2000,5000);
    myTwinkles[twinky].fadeOut =  random(2000,5000);
    myTwinkles[twinky].speedy = random(0);
    myTwinkles[twinky].sideFade = 8;
  }
}


void setupTwinkles5() {
  for (int twinky = 0; twinky < usedTwinkleCount[currentTwinklePattern]; twinky++) {
    myTwinkles[twinky].ledNum = random(numLedsStrip);
    myTwinkles[twinky].rCol =  random(200);
    myTwinkles[twinky].gCol =  random(180);
    myTwinkles[twinky].bCol =  random(200);
    myTwinkles[twinky].rToCol =  random(200);
    myTwinkles[twinky].gToCol =  random(180);
    myTwinkles[twinky].bToCol =  random(200);
    myTwinkles[twinky].start =  random(animLength);
    myTwinkles[twinky].lengthy = random(2000,5000);
    myTwinkles[twinky].widthy =  random(1,10);
    myTwinkles[twinky].fadeIn =  random(2000,5000);
    myTwinkles[twinky].fadeOut =  random(2000,5000);
    myTwinkles[twinky].speedy = random(-20,20);  // larger numbers are slower!
    myTwinkles[twinky].sideFade = 18;
  }
}

void setupTwinkles6() {
  for (int twinky = 0; twinky < usedTwinkleCount[currentTwinklePattern]; twinky++) {
    myTwinkles[twinky].ledNum = random(numLedsStrip);
    myTwinkles[twinky].rCol =  random(220);
    myTwinkles[twinky].gCol =  random(170);
    myTwinkles[twinky].bCol =  random(180);
    myTwinkles[twinky].rToCol =  random(170);
    myTwinkles[twinky].gToCol =  random(140);
    myTwinkles[twinky].bToCol =  random(190);
    myTwinkles[twinky].start =  random(animLength);
    myTwinkles[twinky].lengthy = random(2000,5000);
    myTwinkles[twinky].widthy =  random(1,4);
    myTwinkles[twinky].fadeIn =  random(500,4000);
    myTwinkles[twinky].fadeOut = random(1000,3000);
    myTwinkles[twinky].speedy = random(-14,14);  // larger numbers are slower!
    myTwinkles[twinky].sideFade = random(2,12);
  }
}

