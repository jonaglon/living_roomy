int currentPattern = 6;
int numPatterns = 10;
int twinkleNum=0;

void doLights() {

  if (currentPattern == 0) {
    doAllPatterns();
  } if (currentPattern < 7) {    
    doTwinkles();
  } else if (currentPattern == 7) {
    doFades();
  } else if (currentPattern == 8) {
    doCycles();
  } else if (currentPattern == 9) {
    allOneColor(userColR,userColG,userColB);
  }
}

int cycleWatch=0;
void doAllPatterns() {
  if (cycleWatch != (cycle%6)) {
    cycleWatch=(cycle%6);    
  }
  if (cycleWatch == 0) {
    doFades();
  } else if (cycleWatch == 1) {
    setupTwinkles(1);
  } else if (cycleWatch == 2) {
    setupTwinkles(2);
  } else if (cycleWatch == 3) {
    setupTwinkles(3);
  } else if (cycleWatch == 4) {    
    setupTwinkles(4);
  } else if (cycleWatch == 5) {    
    setupTwinkles(5);
  } else if (cycleWatch == 6) {    
    setupTwinkles(6);
  }
}



void changeLightPattern(int newPatternNum) {
  
  currentPattern = newPatternNum;
  
  if (newPatternNum == 1) {
    setupTwinkles(1);
  } else if (newPatternNum == 2) {
    setupTwinkles(2);
  } else if (newPatternNum == 3) {
    setupTwinkles(3);
  } else if (newPatternNum == 4) {
    setupTwinkles(4);
  } else if (newPatternNum == 5) {
    setupTwinkles(5);
  } else if (newPatternNum == 6) {
    setupTwinkles(6);
  }
}

/////////////////////
// Generic patters //
/////////////////////



void horizontalRainbow(bool includeEyes, bool includeMouth, int speedFactor) {
  int ticko = (timey / speedFactor) % 1024;
  
  for(int j = 0; j < numLedsTotal; j++) {
    int xCoord = (getCoord(j,0)+ticko)%1024;
    setLedDirect(j, wheelR, wheelG, wheelB, 0);    
  }
}
